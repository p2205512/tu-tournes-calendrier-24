class MatriceVigenere {
    constructor() {
      this.matrice = this.genererMatrice();
    }
  
    genererMatrice() {
      const caracteres = "abc'dfghijkCDEFGHéI34è5lnopqrUVWstuv:/.;,z|A B6789=à+-_?!#&{[JKLMNOePQRSçTXY)]}wxymZ012(";
      const matrice = {};
  
      for (let i = 0; i < caracteres.length; i++) {
        const ligne = {};
        for (let j = 0; j < caracteres.length; j++) {
          ligne[caracteres[j]] = caracteres[(i + j) % caracteres.length];
        }
        matrice[caracteres[i]] = ligne;
      }
  
      return matrice;
    }
  
    afficherMatrice() {
      for (const ligne in this.matrice) {
        console.log(ligne, this.matrice[ligne]);
      }
    }
  
    chiffrer(message, cle) {
      let resultat = '';
      const longueurCle = cle.length;
  
      for (let i = 0; i < message.length; i++) {
        const charMessage = message[i];
        const charCle = cle[i % longueurCle];
  
        if (this.matrice[charCle] && this.matrice[charCle][charMessage]) {
          resultat += this.matrice[charCle][charMessage];
        } else {
          // Si le caractère n'est pas dans la matrice, le laisse tel quel
          resultat += charMessage;
        }
      }
  
      return resultat;
    }
  
    dechiffrer(messageChiffre, cle) {
      let resultat = '';
      const longueurCle = cle.length;
  
      for (let i = 0; i < messageChiffre.length; i++) {
        const charChiffre = messageChiffre[i];
        const charCle = cle[i % longueurCle];
  
        for (const char in this.matrice[charCle]) {
          if (this.matrice[charCle][char] === charChiffre) {
            resultat += char;
            break;
          }
        }
      }
  
      return resultat;
    }
  }
  
/*
    PROGRAMME PRINCIPAL
*/

const matriceVigenere = new MatriceVigenere();
const h1 = document.querySelector('h1');

const cle = matriceVigenere.dechiffrer('G7?Khé6_1é6sc', 'tu tournes'); // hé non tu ne trouveras pas la clé comme ça 
const message = matriceVigenere.dechiffrer(")AQe&EçqR1v(lN&(_nC1.|Pzr/çYl?:LpiN}/&CND} {-?[PRvW_7:Qc", 'tu tournes');
const texteModifie = matriceVigenere.chiffrer(message, cle);

h1.textContent = texteModifie;



setInterval(() => {
    const input = document.querySelector('input');
    console.log(input.value);
    if(input.value !== '') h1.textContent = matriceVigenere.dechiffrer(texteModifie, input.value);
    else h1.textContent = matriceVigenere.dechiffrer(texteModifie, ' ');
    
}, 500);